FROM openjdk
ADD target/hello_world-0.0.1-SNAPSHOT.war app.war
ENTRYPOINT ["java","-war","app.war"]
